/* Interlude Skin: -JgC5MOxmP89Q9Dy28Qa/56 */
// simpleButton  V.1
'use strict';

define({
    type: 'button',
    version: '1.0',

    options: {
        clicktext: 'CLICK THE SCREEN',
        color: '#fefb42',
        hideOn: undefined,
        borderthickness: 3,
        borderlength: 50,
        borderdistance: 60,
        borderinitial: -10,
        extraOnboardingTime: 0
    },

    html: (function() { return '' +
    // <MAIN>
    '<div data-ref="main" style="position:absolute;box-sizing:border-box;padding:0px;">' +
    // <CONTAINER>
    '<div data-ref="container" style="position:absolute;height:100%;width:100%;overflow:hidden;">' +
    // <div borderrectscontainer>
    '<div data-ref="borderrectscontainer" style="position:absolute;box-sizing:border-box;height:100%;width:100%">' +
        // <bottomleftcorner>
        '<div data-ref="bottomleftcorner" style="position:absolute;bottom:{{borderinitial}};left:{{borderinitial}};width:{{borderlength}};height:{{borderlength}};border-style:solid;border-width:0px 0px {{borderthickness}}px {{borderthickness}}px;border-color:#FFFFFF">' +
        '</div>' +
        // </bottomleftcorner>
        // <toprightcorner>
        '<div data-ref="toprightcorner" style="position:absolute;top:{{borderinitial}};right:{{borderinitial}};width:{{borderlength}};height:{{borderlength}};border-style:solid;border-width:{{borderthickness}}px {{borderthickness}}px 0px 0px;border-color:#FFFFFF">' +
        '</div>' +
        // </toprightcorner>
    '</div>' +
    // </div borderrectscontainer>

    // <DIV HANDBOUNDINGBOX>
    '<div data-ref="handboundingbox" style="position:absolute;width:346px;height:360px;left:50%;top:50%;-moz-transform:translateX(-50%) translateY(-50%); -webkit-transform:translateX(-50%) translateY(-50%); -o-transform:translateX(-50%) translateY(-50%); -ms-transform:translateX(-50%) translateY(-50%); transform:translateX(-50%) translateY(-50%);color:#FFFFFF;overflow:hidden">' +
        // <DIV HANDANIMBOX>
        '<div data-ref="handanimbox" style="position:absolute;width:100%;height:100%;overflow:visible">' +
            // <DIV handContainer>
            '<div data-ref="handContainer" style="position:absolute;width:166px;height:129px;left:82px;top:102px;overflow:hidden;">' +
                // <SVG HANDSVG>
                '' +
                '<svg data-ref="handsvg" viewbox="-700 -400 2000 2000" xmlns="http://www.w3.org/2000/svg" style="-moz-transform:scale(1);-webkit-transform:scale(1);-o-transform:scale(1);-ms-transform:scale(1);transform:scale(1);overflow:visible">' +
                    // <G HANDWITHOUTSUNFLOWER>
                    '<g data-ref="handwithoutsunflower" fill="#FFFFFF" stroke="#FFFFFF">' +
                    '    <circle data-ref="handcircle" fill="transparent" r="620" cx="300" cy="380" stroke-width="36"></circle>  ' +
                    // <HAND>
                    '    <rect height="36" width="63" x="36" y="297"></rect>' +
                    '    <rect height="36" width="225" x="216" y="684"></rect>' +
                    '    <rect height="36" width="54" x="270" y="162"></rect>' +
                    '    <rect height="36" width="45" x="360" y="198"></rect>' +
                    '    <rect height="36" width="36" x="441" y="234"></rect>' +
                    '    <rect height="36" width="36" x="477" y="270"></rect>' +
                    '    <rect height="36" width="36" x="36" y="387"></rect>' +
                    '    <rect height="36" width="36" x="99" y="333"></rect>' +

                    // <POINTER FINGER>
                            // -> SET y TO DELTA
                    '    <rect data-ref="pointerfinger0" height="36" width="63" x="171"></rect>' +
                            // -> SET width TO (width - DELTA), ADD DELTA TO X
                    '    <rect data-ref="pointerfinger1" height="36" transform="translate(583 -513) rotate(90)"' +
                    '    width="288" x="549" y="313"></rect>' +
                            // -> SET width TO (width - DELTA), ADD DELTA TO X
                    '    <rect data-ref="pointerfinger2" height="36" transform="translate(520 -378) rotate(90)"' +
                    '    width="360" x="414" y="349"></rect>' +
                    // </POINTER FINGER>

                    '    <rect height="36" transform="translate(1096 -558) rotate(90)"' +
                    '    width="216" x="864" y="547"></rect>' +
                    '    <rect height="36" transform=' +
                    '    "translate(-227.5 1507.5) rotate(-90)" width="99" x="886.5" y=' +
                    '    "704.5"></rect>' +
                    '    <rect height="36" transform=' +
                    '    "translate(1280.5 -229.5) rotate(90)" width="99" x="850.5" y=' +
                    '    "803.5"></rect>' +
                    '    <rect height="36" transform=' +
                    '    "translate(1019.5 31.5) rotate(90)" width="99" x="589.5" y="803.5"></rect>' +
                    '    <rect height="36" transform=' +
                    '    "translate(902.5 -13.5) rotate(90)" width="63" x="571.5" y="722.5"></rect>' +
                    '    <rect height="36" transform=' +
                    '    "translate(-547 1089) rotate(-90)" width="72" x="531" y="655"></rect>' +
                    '    <rect height="36" transform=' +
                    '    "translate(695.5 -76.5) rotate(90)" width="63" x="499.5" y="587.5"></rect>' +
                    '    <rect height="36" transform="translate(511 -117) rotate(90)"' +
                    '    width="90" x="414" y="475"></rect>' +
                    '    <rect height="36" transform="translate(754 -522) rotate(90)"' +
                    '    width="126" x="720" y="394"></rect>' +
                    '    <rect height="36" transform="translate(871 -567) rotate(90)"' +
                    '    width="126" x="801" y="430"></rect>' +
                    // </HAND>
                    '</g>' +
                    // </G HANDWITHOUTSUNFLOWER>
                '</svg>' +
                // </SVG HANDSVG>
'           </div>' +
            // <end handContainer>

            // <DIV sunflowerContainer>
            '<div data-ref="sunflowerContainer" style="position:absolute;width:414px;height:397px;left:-43px;top:-20px;overflow:visible">' +
                // <SVG HANDSVG>
                '' +
                '<svg data-ref="sunflowersvg" viewbox="-700 -400 2000 2000" xmlns="http://www.w3.org/2000/svg" style="-moz-transform:scale(0.45);-webkit-transform:scale(0.45);-o-transform:scale(0.45);-ms-transform:scale(0.45);transform:scale(0.45);overflow:visible">' +
                // <SUNFLOWER>
                '   <g data-ref="sunflower" transform="translate(0, -320)">' +
                // From height: 36 width: 36 x: 270 y: -486
                // To height: 300 width: 36 x: 270 y: -750
                '   <rect style="visibility:hidden" fill="#D9A822" height="36" width="36" x="270" y="-486" rx="10" ry="10"></rect>' +
                '   <rect style="visibility:hidden" fill="#D9A822" height="36" width="36" x="270" y="-486" rx="10" ry="10"></rect>' +
                '   <rect style="visibility:hidden" fill="#D9A822" height="36" width="36" x="270" y="-486" rx="10" ry="10"></rect>' +
                '   <rect style="visibility:hidden" fill="#D9A822" height="36" width="36" x="270" y="-486" rx="10" ry="10"></rect>' +
                '   <rect style="visibility:hidden" fill="#D9A822" height="36" width="36" x="270" y="-486" rx="10" ry="10"></rect>' +
                '   <rect style="visibility:hidden" fill="#D9A822" height="36" width="36" x="270" y="-486" rx="10" ry="10"></rect>' +
                '   <rect style="visibility:hidden" fill="#D9A822" height="36" width="36" x="270" y="-486" rx="10" ry="10"></rect>' +
                '   <rect style="visibility:hidden" fill="#D9A822" height="36" width="36" x="270" y="-486" rx="10" ry="10"></rect>' +
                '   </g>' +
                // </SUNFLOWER>
                '</svg>' +
                // </SVG HANDSVG>
'           </div>' +
            // <end sunflowerContainer>

            // <DIV messagetextdiv>
            '<div data-ref="messagetextdiv" style="position:absolute;width:346px;height:175px;top:84px;color:#FFFFFF;overflow:hidden">' +
                // <DIV clicktextContainer>
                '<div data-ref="clicktextContainer" style="position:absolute;width:346px;height:47px;top:137px;color:#FFFFFF;overflow:hidden">' +
                    // <SVG clicktextsvg>' +
    '                <svg data-ref="clicktextsvg" style="position:absolute;top:0px;display:none" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"' +
    '                     viewBox="-115 -27  650 65" enable-background="new 0 0 399 38.7" xml:space="preserve">' +
    '                <g>' +
    '                    <path fill="#FFFFFF" d="M26,7.5c0.5,2.3-0.2,4.2-1.8,5.6c-1.1-0.7-2.9-1.3-5.3-1.7c-2.1-0.2-3.9,0.4-5.2,1.8' +
    '                        c-1.3,1.4-2.2,3.3-2.5,5.6c-0.4,1.4-0.2,3.3,0.8,5.6c1,2.1,2.5,3.1,4.5,3.3s4.5-0.3,7.3-1.2c1,2,1.1,4,0.3,5.9' +
    '                        c-3.3,1.5-6.1,2.3-8.1,2.2c-3.8-0.5-6.6-1.9-8.4-4.2c-2.3-3.1-3.1-7.4-2.4-13.1c0.6-4.5,2-7.8,4.2-10.1c2.9-2.4,6.4-3.3,10.6-2.9' +
    '                        C22.8,4.5,24.8,5.5,26,7.5z"/>' +
    '                    <path fill="#FFFFFF" d="M38.7,4.9l-1.4,22.6L49,27.1c1.1,2.3,0.9,4.5-0.4,6.5l-17,0.6c-0.6-0.1-0.9-0.5-1-1.2l1.6-27.8' +
    '                        C33.6,4.5,35.8,4.3,38.7,4.9z"/>' +
    '                    <path fill="#FFFFFF" d="M57.1,4.9c2-0.5,4.1-0.4,6.4,0.3l-2.3,28.7c-2.7,0.8-4.9,0.7-6.7-0.3L57.1,4.9z"/>' +
    '                    <path fill="#FFFFFF" d="M89.3,7.5c0.5,2.3-0.2,4.2-1.8,5.6c-1.1-0.7-2.9-1.3-5.3-1.7c-2.1-0.2-3.9,0.4-5.2,1.8' +
    '                        c-1.3,1.4-2.2,3.3-2.5,5.6c-0.4,1.4-0.2,3.3,0.8,5.6c1,2.1,2.5,3.1,4.5,3.3s4.5-0.3,7.3-1.2c1,2,1.1,4,0.3,5.9' +
    '                        c-3.3,1.5-6.1,2.3-8.1,2.2c-3.8-0.5-6.6-1.9-8.4-4.2c-2.3-3.1-3.1-7.4-2.4-13.1c0.6-4.5,2-7.8,4.2-10.1c2.9-2.4,6.4-3.3,10.6-2.9' +
    '                        C86.2,4.5,88.2,5.5,89.3,7.5z"/>' +
    '                    <path fill="#FFFFFF" d="M102,4.8l-0.6,8.8l8.9-9c2.5-0.2,4.1,0.9,5,3.3l-10.2,10.2l9.8,11.8c-0.2,1.4-0.8,2.4-1.6,3.1' +
    '                        s-2.2,0.9-4,0.6l-8.5-11.3l-0.4,11c-1.9,1.1-4,1-6.4-0.1l1.4-28.4C96.9,3.9,99.1,3.9,102,4.8z"/>' +
    '                    <path fill="#FFFFFF" d="M154.8,4.7c0.9,2.1,0.8,4.2-0.2,6.3l-7.2,0.1l-1.1,22.7c-2,0.8-4.1,0.8-6.5,0l1.3-22.4l-7.1,0.2' +
    '                        c-1.4-2.4-1.5-4.5-0.2-6.2L154.8,4.7z"/>' +
    '                    <path fill="#FFFFFF" d="M168,5.2l-0.6,10.9l9.3-0.5l0.7-10.2c2-1,3.9-1,5.8,0l-1.3,27.8c-2.4,1.2-4.5,1.3-6.3,0.2l0.6-11.9' +
    '                        l-9.2,0.4l-0.7,11.9c-2.8,0.7-4.9,0.6-6.3-0.5l1.7-27.4C163.6,4.5,165.7,4.3,168,5.2z"/>' +
    '                    <path fill="#FFFFFF" d="M208.2,4.6c0.6,2,0.5,4-0.3,5.7l-12.8,0.4l-0.4,5l10.5-0.2c1,1.9,0.9,3.7-0.2,5.4l-10.7,0.4l-0.5,6.1' +
    '                        l13.1-0.4c1.1,2.3,0.9,4.5-0.4,6.5l-18.3,0.7c-0.6-0.1-0.9-0.5-1-1.2l1.8-26.8c0.1-0.5,0.4-0.9,0.9-1.1L208.2,4.6z"/>' +
    '                    <path fill="#FFFFFF" d="M227,30.9c-0.8-3-0.4-5.2,1.5-6.9c3.3,2,5.8,3.2,7.3,3.6c2.8,0,4.4-0.7,4.6-2.1c0.1-1.1-0.8-2.2-2.6-3.2' +
    '                        l-5-2.4c-3.1-1.7-4.8-4-4.9-7.1c0.1-1.7,0.7-3.3,1.8-4.7c1.1-1.4,2.3-2.3,3.7-2.7c1.4-0.4,3-0.6,4.9-0.5c3.4,0.2,6.5,1.2,9,2.9' +
    '                        c0.1,2.5-0.7,4.6-2.3,6.4c-3.2-2.4-5.7-3.5-7.6-3.3c-1.8-0.2-2.7,0.4-2.8,1.9c-0.1,0.8,1.2,1.9,3.8,3.3c3,1.2,5.1,2.6,6.6,4.1' +
    '                        c1.4,1.5,2.1,3.4,1.9,5.6c-0.2,3-1.6,5.3-4.2,6.9c-1.6,1.1-4,1.6-7.1,1.4C232.5,33.8,229.7,32.7,227,30.9z"/>' +
    '                    <path fill="#FFFFFF" d="M273.2,7.5c0.5,2.3-0.2,4.2-1.8,5.6c-1.1-0.7-2.9-1.3-5.3-1.7c-2.1-0.2-3.9,0.4-5.2,1.8' +
    '                        c-1.3,1.4-2.2,3.3-2.5,5.6c-0.4,1.4-0.2,3.3,0.8,5.6c1,2.1,2.5,3.1,4.5,3.3s4.5-0.3,7.3-1.2c1,2,1.1,4,0.3,5.9' +
    '                        c-3.3,1.5-6.1,2.3-8.1,2.2c-3.8-0.5-6.6-1.9-8.4-4.2c-2.3-3.1-3.1-7.4-2.4-13.1c0.6-4.5,2-7.8,4.2-10.1c2.9-2.4,6.4-3.3,10.6-2.9' +
    '                        C270.1,4.5,272.1,5.5,273.2,7.5z"/>' +
    '                    <path fill="#FFFFFF" d="M282.3,5.1c6.5-0.6,11-0.7,13.6-0.3c2.6,0.4,4.2,1.6,4.9,3.6s0.7,4.6,0.1,7.9c-0.6,3.3-3,5.5-7.2,6.5' +
    '                        l6.9,8.9c-1.3,2.6-3.4,3.4-6.4,2.5l-6.9-9.9l-2.3,0.2l-0.9,9.2c-2.7,0.9-4.8,0.7-6.4-0.6l2.5-26.7L282.3,5.1z M286.4,10.5l-0.8,8.1' +
    '                        c5.4-0.4,8.4-1.3,9-2.9s0.7-2.9,0.1-4.2C294.2,10.3,291.4,10,286.4,10.5z"/>' +
    '                    <path fill="#FFFFFF" d="M326.8,4.6c0.6,2,0.5,4-0.3,5.7l-12.8,0.4l-0.4,5l10.5-0.2c1,1.9,0.9,3.7-0.2,5.4l-10.7,0.4l-0.5,6.1' +
    '                        l13.1-0.4c1.1,2.3,0.9,4.5-0.4,6.5l-18.3,0.7c-0.6-0.1-0.9-0.5-1-1.2l1.8-26.8c0.1-0.5,0.4-0.9,0.9-1.1L326.8,4.6z"/>' +
    '                    <path fill="#FFFFFF" d="M352.6,4.6c0.6,2,0.5,4-0.3,5.7l-12.8,0.4l-0.4,5l10.5-0.2c1,1.9,0.9,3.7-0.2,5.4l-10.7,0.4l-0.5,6.1' +
    '                        l13.1-0.4c1.1,2.3,0.9,4.5-0.4,6.5l-18.3,0.7c-0.6-0.1-0.9-0.5-1-1.2l1.8-26.8c0.1-0.5,0.4-0.9,0.9-1.1L352.6,4.6z"/>' +
    '                    <path fill="#FFFFFF" d="M364.7,5.1l9.3,15.8l0.9-15.4c2-1,3.9-1,5.8,0L379,33.4c-1.3,1.2-3.1,1.3-5.4,0.4l-8.7-15.9l-1.1,16' +
    '                        c-2.8,0.7-4.9,0.6-6.3-0.5l1.7-27.4C360,4.7,361.9,4.4,364.7,5.1z"/>' +
    '                    <path fill="#FFFFFF" d="M387.7,26.9c0.5,0,1.1,0.1,1.6,0.4s0.9,0.7,1.2,1.2s0.4,1,0.4,1.6c0,0.5-0.1,1.1-0.4,1.6s-0.7,0.9-1.2,1.2' +
    '                        s-1,0.4-1.6,0.4c-0.6,0-1.1-0.1-1.6-0.4s-0.9-0.7-1.2-1.2s-0.4-1-0.4-1.6c0-0.6,0.1-1.1,0.4-1.6s0.7-0.9,1.2-1.2' +
    '                        S387.2,26.9,387.7,26.9z M387.5,4.4l-1,20.4c1.1,0.7,2.5,0.8,4.2,0.3l3.9-19.8C392.6,4,390.2,3.7,387.5,4.4z"/>' +
    '                </svg> ' +
                    // </SVG clicktextsvg>
    '           </div>' +
                // </end clicktextContainer>

                // <DIV awesometextContainer>
                '<div data-ref="awesometextContainer" style="position:absolute;width:329px;height:80px;top:48px;left:2px;color:#FFFFFF;overflow:hidden">' +
                    // <SVG awesometextsvg>
'                    <svg data-ref="awesometextsvg" style="position:absolute;top:19px;display:none" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"' +
'                         viewBox="-94 0 400 80" enable-background="new 0 0 202.3 38.7" xml:space="preserve">' +
'                    <g>' +
'                        <path fill="#EDD8BB" d="M20.5,4.5l7.8,28.1c-1.8,1.2-4.1,1.5-7,0.9L20,28.3l-7.2,0.9l-1.8,5.3c-2.9,0.4-5.1,0-6.5-1.2L14.7,4.7' +
'                            C16.7,3.4,18.7,3.3,20.5,4.5z M17.2,13.8l-3.1,9.6l4.8-0.6L17.2,13.8z"/>' +
'                        <path fill="#EDD8BB" d="M48.3,33.9l-2.7-16.7L41,34.2c-2.1,0.5-4.2,0.5-6.2,0.1L32.2,4.8c1.9-0.6,4.1-0.6,6.6-0.2l0.8,16.7l4-16.5' +
'                            c2.3-0.5,4.3-0.4,5.8,0.2l1.8,16.8l3.1-17.1c2-0.7,4-0.6,6,0.3l-4.9,28.7C53.6,34.5,51.2,34.5,48.3,33.9z"/>' +
'                        <path fill="#EDD8BB" d="M85.2,4.6c0.6,2,0.5,4-0.3,5.7l-12.8,0.4l-0.4,5l10.5-0.2c1,1.9,0.9,3.7-0.2,5.4l-10.7,0.4l-0.5,6.1' +
'                            l13.1-0.4c1.1,2.3,0.9,4.5-0.4,6.5l-18.3,0.7c-0.6-0.1-0.9-0.5-1-1.2l1.8-26.8c0.1-0.5,0.4-0.9,0.9-1.1L85.2,4.6z"/>' +
'                        <path fill="#EDD8BB" d="M91.2,30.9c-0.8-3-0.4-5.2,1.5-6.9c3.3,2,5.8,3.2,7.3,3.6c2.8,0,4.4-0.7,4.6-2.1c0.1-1.1-0.8-2.2-2.6-3.2' +
'                            l-5-2.4c-3.1-1.7-4.8-4-4.9-7.1c0.1-1.7,0.7-3.3,1.8-4.7c1.1-1.4,2.3-2.3,3.7-2.7c1.4-0.4,3-0.6,4.9-0.5c3.4,0.2,6.5,1.2,9,2.9' +
'                            c0.1,2.5-0.7,4.6-2.3,6.4c-3.2-2.4-5.7-3.5-7.6-3.3c-1.8-0.2-2.7,0.4-2.8,1.9c-0.1,0.8,1.2,1.9,3.8,3.3c3,1.2,5.1,2.6,6.6,4.1' +
'                            c1.4,1.5,2.1,3.4,1.9,5.6c-0.2,3-1.6,5.3-4.2,6.9c-1.6,1.1-4,1.6-7.1,1.4C96.8,33.8,94,32.7,91.2,30.9z"/>' +
'                        <path fill="#EDD8BB" d="M130.2,4.6c3.6,0,6.4,1.4,8.5,4.2c2.1,2.8,3,6.3,2.6,10.6c-0.4,4.3-1.8,7.8-4.3,10.6' +
'                            c-2.5,2.8-5.5,4.2-9.1,4.2s-6.6-1.4-8.9-4.1s-3.3-6.2-2.9-10.5s1.9-7.8,4.6-10.7C123.4,6.1,126.6,4.6,130.2,4.6z M129.6,12' +
'                            c-1.7,0-3.3,0.7-4.6,2.1c-1.4,1.4-2.2,3.3-2.5,5.6s0.2,4.2,1.4,5.8s2.8,2.2,4.5,2.2c1.8-0.1,3.3-0.9,4.5-2.3' +
'                            c1.2-1.5,1.9-3.3,2.1-5.4c0.2-2.2-0.2-4-1.3-5.5C132.7,12.7,131.3,12,129.6,12z"/>' +
'                        <path fill="#EDD8BB" d="M154.2,5.1l4.6,16.4l8-16.4c1.9-0.5,3.7-0.5,5.4,0l-1.8,29l-6.3,0.1l0.8-13.9l-6,13.6' +
'                            c-1,0.5-1.9,0.3-2.6-0.5L153.1,20l-1,14l-6.3-0.1l2-29C149.3,4.3,151.5,4.3,154.2,5.1z"/>' +
'                        <path fill="#EDD8BB" d="M197.1,4.6c0.6,2,0.5,4-0.3,5.7L184,10.8l-0.4,5l10.5-0.2c1,1.9,0.9,3.7-0.2,5.4l-10.7,0.4l-0.5,6.1' +
'                            l13.1-0.4c1.1,2.3,0.9,4.5-0.4,6.5l-18.3,0.7c-0.6-0.1-0.9-0.5-1-1.2l1.8-26.8c0.1-0.5,0.4-0.9,0.9-1.1L197.1,4.6z"/>' +
'                    </g>' +
'                    </svg>' +
                // </SVG awesometextsvg>

                // <SVG clickanytimesvg>' +
'                <svg data-ref="clickanytimesvg" style="position:absolute;top:0;display:none" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"' +
'                         viewBox="-21 0 400 80" enable-background="new 0 0 202.3 38.7" xml:space="preserve">' +
'                <g>' +
'                    <path fill="#EDD8BB" d="M48.2,7.5c0.5,2.3-0.2,4.2-1.8,5.6c-1.1-0.7-2.9-1.3-5.3-1.7c-2.1-0.2-3.9,0.4-5.2,1.8' +
'                        c-1.3,1.4-2.2,3.3-2.5,5.6c-0.4,1.4-0.2,3.3,0.8,5.6c1,2.1,2.5,3.1,4.5,3.3c2,0.1,4.4-0.3,7.3-1.2c1,2,1.1,4,0.3,5.9' +
'                        c-3.3,1.5-6.1,2.3-8.1,2.2c-3.8-0.5-6.6-1.9-8.4-4.2c-2.3-3.1-3.1-7.4-2.4-13.1c0.6-4.5,2-7.8,4.2-10.1c2.9-2.4,6.4-3.3,10.6-2.9' +
'                        C45,4.5,47,5.5,48.2,7.5z"/>' +
'                    <path fill="#EDD8BB" d="M60.9,4.9l-1.4,22.6l11.7-0.3c1.1,2.3,0.9,4.5-0.4,6.5l-17,0.6c-0.6-0.1-0.9-0.5-1-1.2l1.6-27.8' +
'                        C55.8,4.5,58,4.3,60.9,4.9z"/>' +
'                    <path fill="#EDD8BB" d="M79.3,4.9c2-0.5,4.1-0.4,6.4,0.3l-2.3,28.7c-2.7,0.8-4.9,0.7-6.7-0.3L79.3,4.9z"/>' +
'                    <path fill="#EDD8BB" d="M111.5,7.5c0.5,2.3-0.2,4.2-1.8,5.6c-1.1-0.7-2.9-1.3-5.3-1.7c-2.1-0.2-3.9,0.4-5.2,1.8' +
'                        c-1.3,1.4-2.2,3.3-2.5,5.6c-0.4,1.4-0.2,3.3,0.8,5.6c1,2.1,2.5,3.1,4.5,3.3c2,0.1,4.4-0.3,7.3-1.2c1,2,1.1,4,0.3,5.9' +
'                        c-3.3,1.5-6.1,2.3-8.1,2.2c-3.8-0.5-6.6-1.9-8.4-4.2c-2.3-3.1-3.1-7.4-2.4-13.1c0.6-4.5,2-7.8,4.2-10.1c2.9-2.4,6.4-3.3,10.6-2.9' +
'                        C108.4,4.5,110.4,5.5,111.5,7.5z"/>' +
'                    <path fill="#EDD8BB" d="M124.2,4.8l-0.6,8.8l8.9-9c2.5-0.2,4.1,0.9,5,3.3l-10.2,10.2l9.8,11.8c-0.2,1.4-0.8,2.4-1.6,3.1' +
'                        s-2.2,0.9-4,0.6l-8.5-11.3l-0.4,11c-1.9,1.1-4,1-6.4-0.1l1.4-28.4C119.1,3.9,121.3,3.9,124.2,4.8z"/>' +
'                    <path fill="#EDD8BB" d="M170.2,4.5l7.8,28.1c-1.8,1.2-4.1,1.5-7,0.9l-1.4-5.2l-7.2,0.9l-1.8,5.3c-2.9,0.4-5.1,0-6.5-1.2l10.2-28.5' +
'                        C166.4,3.4,168.3,3.3,170.2,4.5z M166.8,13.8l-3.1,9.6l4.8-0.6L166.8,13.8z"/>' +
'                    <path fill="#EDD8BB" d="M189.1,5.1l9.3,15.8l0.9-15.4c2-1,3.9-1,5.8,0l-1.7,27.9c-1.3,1.2-3.1,1.3-5.4,0.4l-8.7-15.9l-1.1,16' +
'                        c-2.8,0.7-4.9,0.6-6.3-0.5l1.7-27.4C184.4,4.7,186.3,4.4,189.1,5.1z"/>' +
'                    <path fill="#EDD8BB" d="M215.1,4.6l4.7,10.1l6.2-10c2.8-0.1,4.7,0.5,5.7,1.9l-9.2,13.7L221.9,34c-2.4,0.8-4.5,0.8-6.3,0l0.5-13.9' +
'                        l-7.2-13.4C210.3,5.1,212.3,4.4,215.1,4.6z"/>' +
'                    <path fill="#EDD8BB" d="M256.8,4.7c0.9,2.1,0.8,4.2-0.2,6.3l-7.2,0.1l-1.1,22.7c-2,0.8-4.1,0.8-6.5,0l1.3-22.4l-7.1,0.2' +
'                        c-1.4-2.4-1.5-4.5-0.2-6.2L256.8,4.7z"/>' +
'                    <path fill="#EDD8BB" d="M262.2,4.9c2-0.5,4.1-0.4,6.4,0.3l-2.3,28.7c-2.7,0.8-4.9,0.7-6.7-0.3L262.2,4.9z"/>' +
'                    <path fill="#EDD8BB" d="M281.8,5.1l4.6,16.4l8-16.4c1.9-0.5,3.7-0.5,5.4,0L298,34l-6.3,0.1l0.8-13.9l-6,13.6' +
'                        c-1,0.5-1.9,0.3-2.6-0.5L280.6,20l-1,14l-6.3-0.1l2-29C276.9,4.3,279.1,4.3,281.8,5.1z"/>' +
'                    <path fill="#EDD8BB" d="M324.7,4.6c0.6,2,0.5,4-0.3,5.7l-12.8,0.4l-0.4,5l10.5-0.2c1,1.9,0.9,3.7-0.2,5.4l-10.7,0.4l-0.5,6.1' +
'                        l13.1-0.4c1.1,2.3,0.9,4.5-0.4,6.5l-18.3,0.7c-0.6-0.1-0.9-0.5-1-1.2l1.8-26.8c0.1-0.5,0.4-0.9,0.9-1.1L324.7,4.6z"/>' +
'                    <path fill="#EDD8BB" d="M26.7,46.7c0.9,2.1,0.8,4.2-0.2,6.3L19.4,53l-1.1,22.7c-2,0.8-4.1,0.8-6.5,0l1.3-22.4L6,53.4' +
'                        c-1.4-2.4-1.5-4.5-0.2-6.2L26.7,46.7z"/>' +
'                    <path fill="#EDD8BB" d="M43.3,46.6c3.6,0,6.4,1.4,8.5,4.2s3,6.3,2.6,10.6s-1.8,7.8-4.3,10.6c-2.5,2.8-5.5,4.2-9.1,4.2' +
'                        s-6.6-1.4-8.9-4.1c-2.3-2.7-3.3-6.2-2.9-10.5c0.4-4.3,1.9-7.8,4.6-10.7C36.6,48.1,39.8,46.6,43.3,46.6z M42.8,54' +
'                        c-1.7,0-3.3,0.7-4.6,2.1c-1.4,1.4-2.2,3.3-2.5,5.6c-0.3,2.3,0.2,4.2,1.4,5.8s2.8,2.2,4.5,2.2c1.8-0.1,3.3-0.9,4.5-2.3' +
'                        c1.2-1.5,1.9-3.3,2.1-5.4c0.2-2.2-0.2-4-1.3-5.5C45.9,54.7,44.5,54,42.8,54z"/>' +
'                    <path fill="#EDD8BB" d="M72.9,72.9c-0.8-3-0.4-5.2,1.5-6.9c3.3,2,5.8,3.2,7.3,3.6c2.8,0,4.4-0.7,4.6-2.1c0.1-1.1-0.8-2.2-2.6-3.2' +
'                        l-5-2.4c-3.1-1.7-4.8-4-4.9-7.1c0.1-1.7,0.7-3.3,1.8-4.7c1.1-1.4,2.3-2.3,3.7-2.7c1.4-0.4,3-0.6,4.9-0.5c3.4,0.2,6.5,1.2,9,2.9' +
'                        c0.1,2.5-0.7,4.6-2.3,6.4c-3.2-2.4-5.7-3.5-7.6-3.3c-1.8-0.2-2.7,0.4-2.8,1.9c-0.1,0.8,1.2,1.9,3.8,3.3c3,1.2,5.1,2.6,6.6,4.1' +
'                        s2.1,3.4,1.9,5.6c-0.2,3-1.6,5.3-4.2,6.9c-1.6,1.1-4,1.6-7.1,1.4C78.4,75.8,75.6,74.7,72.9,72.9z"/>' +
'                    <path fill="#EDD8BB" d="M113.3,75.9l-2.7-16.7L106,76.2c-2.1,0.5-4.2,0.5-6.2,0.1l-2.6-29.6c1.9-0.6,4.1-0.6,6.6-0.2l0.8,16.7' +
'                        l4-16.5c2.3-0.5,4.3-0.4,5.8,0.2l1.8,16.8l3.1-17.1c2-0.7,4-0.6,6,0.3l-4.9,28.7C118.5,76.5,116.2,76.5,113.3,75.9z"/>' +
'                    <path fill="#EDD8BB" d="M131.7,46.9c2-0.5,4.1-0.4,6.4,0.3l-2.3,28.7c-2.7,0.8-4.9,0.7-6.7-0.3L131.7,46.9z"/>' +
'                    <path fill="#EDD8BB" d="M164.9,46.7c0.9,2.1,0.8,4.2-0.2,6.3l-7.2,0.1l-1.1,22.7c-2,0.8-4.1,0.8-6.5,0l1.3-22.4l-7.1,0.2' +
'                        c-1.4-2.4-1.5-4.5-0.2-6.2L164.9,46.7z"/>' +
'                    <path fill="#EDD8BB" d="M188.1,49.5c0.5,2.3-0.2,4.2-1.8,5.6c-1.1-0.7-2.9-1.3-5.3-1.7c-2.1-0.2-3.9,0.4-5.2,1.8' +
'                        c-1.3,1.4-2.2,3.3-2.5,5.6c-0.4,1.4-0.2,3.3,0.8,5.6c1,2.1,2.5,3.1,4.5,3.3c2,0.1,4.4-0.3,7.3-1.2c1,2,1.1,4,0.3,5.9' +
'                        c-3.3,1.5-6.1,2.3-8.1,2.2c-3.8-0.5-6.6-1.9-8.4-4.2c-2.3-3.1-3.1-7.4-2.4-13.1c0.6-4.5,2-7.8,4.2-10.1c2.9-2.4,6.4-3.3,10.6-2.9' +
'                        C184.9,46.5,186.9,47.5,188.1,49.5z"/>' +
'                    <path fill="#EDD8BB" d="M200.6,47.2l-0.6,10.9l9.3-0.5l0.7-10.2c2-1,3.9-1,5.8,0l-1.3,27.8c-2.4,1.2-4.5,1.3-6.3,0.2l0.6-11.9' +
'                        l-9.2,0.4l-0.7,11.9c-2.8,0.7-4.9,0.6-6.3-0.5l1.7-27.4C196.2,46.5,198.3,46.3,200.6,47.2z"/>' +
'                    <path fill="#EDD8BB" d="M248.5,46.5l7.8,28.1c-1.8,1.2-4.1,1.5-7,0.9l-1.4-5.2l-7.2,0.9l-1.8,5.3c-2.9,0.4-5.1,0-6.5-1.2l10.2-28.5' +
'                        C244.7,45.4,246.7,45.3,248.5,46.5z M245.2,55.8l-3.1,9.6l4.8-0.6L245.2,55.8z"/>' +
'                    <path fill="#EDD8BB" d="M279.9,59.8c0.8,0.4,1.3,1,1.6,1.8l-0.8,9.3c-1.3,2.3-2.9,3.9-4.9,4.8c-1.8,0.8-3.6,1.2-5.4,1.2' +
'                        c-3.3,0-5.9-1-7.8-3.1c-2.7-3-3.9-7.2-3.5-12.7c0.4-5.3,2.1-9.3,5.2-11.8c2.5-1.7,5.5-2.5,8.9-2.5c1.9,0,3.4,0.2,4.6,0.6' +
'                        s2.5,1.3,3.8,2.6c0.1,2.3-0.4,4.2-1.7,5.6l-2-0.9c-1.5-0.9-3.3-1.4-5.1-1.4c-2.5,0.1-4.3,0.6-5.4,1.7s-1.8,3.3-2.1,6.6' +
'                        c-0.2,2.8,0,4.9,0.7,6.4c0.7,2.1,2.3,3,4.8,2.9c2.7-0.3,4.1-1.3,4.4-2.9l0.1-2l-4,0c-1.2-2.3-1-4.3,0.6-6.1L279.9,59.8z"/>' +
'                    <path fill="#EDD8BB" d="M300.2,46.5l7.8,28.1c-1.8,1.2-4.1,1.5-7,0.9l-1.4-5.2l-7.2,0.9l-1.8,5.3c-2.9,0.4-5.1,0-6.5-1.2l10.2-28.5' +
'                        C296.4,45.4,298.4,45.3,300.2,46.5z M296.9,55.8l-3.1,9.6l4.8-0.6L296.9,55.8z"/>' +
'                    <path fill="#EDD8BB" d="M314.5,46.9c2-0.5,4.1-0.4,6.4,0.3l-2.3,28.7c-2.7,0.8-4.9,0.7-6.7-0.3L314.5,46.9z"/>' +
'                    <path fill="#EDD8BB" d="M332,47.1l9.3,15.8l0.9-15.4c2-1,3.9-1,5.8,0l-1.7,27.9c-1.3,1.2-3.1,1.3-5.4,0.4l-8.7-15.9l-1.1,16' +
'                        c-2.8,0.7-4.9,0.6-6.3-0.5l1.7-27.4C327.4,46.6,329.2,46.4,332,47.1z"/>' +
'                </g>' +
'                </svg>' +
                // </SVG clickanytimesvg>
'           </div>' +
            // </end awesometextContainer>
            '</div>' +
            // </DIV messagetextdiv>

        '</div>' +
        // </DIV HANDANIMBOX>
    '</div>' +
    // </DIV HANDBOUNDINGBOX>


    '<div data-ref="hit" style="position:absolute;width:100%;height:100%;top:0;left:0;cursor:pointer;"></div>' +
    '</div>' +
    // </CONTAINER>
    '</div>';
})(),
    // </MAIN>


    init: function() {
        this.initalProps = {};
        this.handClickDelta = 60;
        this.handDivStart = 152;
        this.handDivFinish = 0;
        this.clickDivStart = 24;
        this.clickDivFinish = 0;
        this.handBottomY = 125;
        this.handBottomScale = 0.6;
        this.opacityCycleSpeed = 0.5;
        // the 5 is because the node starts with the transition in
        this.delayBeforeOnboarding = 2.2 + (this.options.extraOnboardingTime || 0);
        this.delayUntilHandMove = 4;
        this.inactiveLength = 10;
        this.currentPhase = 'INIT'; // INIT, RENDERED, BEGINONBOARD, HANDINCENTER, HANDSLIDINGDOWN, HANDONBOTTOM, DISABLED, INACTIVEHAND
        this.phasesVisited = [];
        this.animationsSetup = false;

        this.onNodeStart = function (node) {
            this.restartInactiveTimeout(this);
            if (this.options.nodesToShow.indexOf(node.id) > -1) {
                console.log('calling onRender inside nodestart, animationsSetup is ', this.animationsSetup);
                this.ui.hit.parentNode.style['pointer-events'] = 'auto';
                this.switchPhaseFactory('RENDERED', 310)();
                if (this.animationsSetup === false) {
                    this.setupAndAnimate(this);
                }
                else {
                    this.switchPhaseFactory('DISABLED', 315)();
                    this.restartInactiveTimeout(this);
                }
                // disable inactive hand during decisions
                if (node.data && node.data.decision && node.data.decision.startTime) {
                    var timeupdateString = 'timeupdate:'.concat(node.data.decision.startTime);
                    node.on(timeupdateString, function () {
                        this.restartInactiveTimeout(this, 'kill');
                    }.bind(this));
                }
                node.on('timeupdate:-6', function() {
                    this.restartInactiveTimeout(this, 'kill');
                }.bind(this));
            }
            else {
                this.switchPhaseFactory('REPLAY', 330)();
                this.restartInactiveTimeout(this, 'kill');
            }
        }.bind(this);

        this.onNodeEnd = function() {
            this.restartInactiveTimeout(this, 'kill');
            // this.switchPhaseFactory('OFFSCREEN', 7)();
        }.bind(this);

        this.onEnded = function() {
            this.restartInactiveTimeout(this, 'kill');
            this.switchPhaseFactory('OFFSCREEN', 370)();
        }.bind(this);

        this.onPause = function() {
            if (!this.ctx.player.currentNode || this.options.nodesToShow.indexOf(this.ctx.player.currentNode.id) < 0) {
                return;
            }

            if (this.timeline0Master && this.timeline0Master.isActive()) {
                this.timeline0Master.pause();
                this.ctx.player.once('play', function() {
                    if (this.timeline0Master && this.timeline0Master.paused()) {
                        this.timeline0Master.play();
                    }
                }.bind(this));
            }

            this.restartInactiveTimeout(this, 'pause');
            this.ctx.player.once('play', function() {
                this.restartInactiveTimeout(this);
            }.bind(this));
        }.bind(this);

        this.onNodeDecisionEnd = function() {
            this.ui.hit.parentNode.style['pointer-events'] = 'none';
            this.ctx.player.switch(0);
        }.bind(this);
    },

    onRender: function() {
        this.ctx.player.on('nodestart', this.onNodeStart, 'menuBackgroundButton');
        this.ctx.player.on('nodeend', this.onNodeEnd, 'menuBackgroundButton');
        this.ctx.player.on('showingendloop', this.onEnded, 'menuBackgroundButton');
        if (this.options.nodesToShow && this.options.nodesToShow.length) {
            this.options.nodesToShow.map(function(id) { return this.ctx.player.repository.get(id); }.bind(this)).forEach(function(node) {
                node.on('decision.end', this.onNodeDecisionEnd, 'menuBackgroundButton');
            }.bind(this));
        }
    },

    setupAndAnimate: function(btnObject) {
        btnObject.duplicateRedContainer(btnObject);

        btnObject.switchPhaseFactory('RENDERED', 3)();
        btnObject.timeline0Master = new window.TimelineMax({ });

        // Run beginning animations!
        btnObject.timeline0Master.add(btnObject.makeTimelineShowCornersAndHand(btnObject), 'timeline1CornersAndHand')
                                .add(btnObject.makeTimelineMoveHandToBottom(btnObject), 'timeline2HandToBottom');

        btnObject.inactiveTimeline = btnObject.makeTimelineInactiveHand(btnObject);
        btnObject.inactiveTimeline.pause();

        btnObject.ui.hit.addEventListener('click', function() {
            btnObject.ctx.player.switch('next');
            btnObject.handleClick(btnObject, btnObject.currentPhase);
        }.bind(btnObject));

        btnObject.ctx.player.on('pause', btnObject.onPause);

        btnObject.animationsSetup = true;
    },

    duplicateRedContainer: function(btnObject) {
        var duplicateContainer = btnObject.ui.container.cloneNode(true);
        duplicateContainer.style.left = 2;
        duplicateContainer.style.pointerEvents = 'none';
        btnObject.ui.container2 = duplicateContainer;
        btnObject.ui.container.parentNode.insertBefore(duplicateContainer, btnObject.ui.container);

        var allChildren = btnObject.ui.container2.getElementsByTagName('*');

        for (var i = -1, l = allChildren.length; ++i < l;) {
            if (allChildren[i].getAttribute('fill') && allChildren[i].getAttribute('fill') !== 'transparent') {
                allChildren[i].setAttribute('fill', '#7A1216');
            }
            if (allChildren[i].getAttribute('color')) {
                allChildren[i].setAttribute('color', '#7A1216');
            }
            if (allChildren[i].getAttribute('stroke')) {
                allChildren[i].setAttribute('stroke', '#7A1216');
            }
            if (allChildren[i].style.borderColor) {
                allChildren[i].style.borderColor = '#7A1216';
            }

            var dataRefName = allChildren[i].getAttribute('data-ref');
            if (dataRefName) {
                btnObject.ui[dataRefName + 'Copy'] = allChildren[i];
            }
        }
        btnObject.animate(btnObject.ui.handsvg, 0, { y: btnObject.handDivStart });
        btnObject.animate(btnObject.ui.handsvgCopy, 0, { y: btnObject.handDivStart });
        btnObject.animate(btnObject.ui.clicktextContainer, 0, { y: btnObject.clickDivStart });
        btnObject.animate(btnObject.ui.clicktextContainerCopy, 0, { y: btnObject.clickDivStart });
    },

    restartInactiveTimeout: function(btnObject, keyword) {
        console.log('restarting restartInactiveTimeout', keyword);
        if (btnObject.inactiveTimeout) {
            clearTimeout(btnObject.inactiveTimeout);
        }
        if (btnObject.inactiveTimeline) {
            btnObject.inactiveTimeline.pause();
        }
        if (keyword && keyword === 'pause') {
            btnObject.inactiveTimeout = undefined;
            return;
        }
        if (keyword && keyword === 'kill' && btnObject.animationsSetup) {
            console.log('KILLING INACTIVEHAND', 438);
            btnObject.animate(btnObject.ui.handwithoutsunflower, 0, { display: 'none' });
            btnObject.animate(btnObject.ui.handwithoutsunflowerCopy, 0, { display: 'none' });
            btnObject.animate(btnObject.ui.toprightcorner, 0, { top:this.options.borderinitial, right:this.options.borderinitial });
            btnObject.animate(btnObject.ui.toprightcornerCopy, 0, { top:this.options.borderinitial, right:this.options.borderinitial });
            btnObject.animate(btnObject.ui.bottomleftcorner, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial });
            btnObject.animate(btnObject.ui.bottomleftcornerCopy, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial });
            btnObject.toggleOpacityCycle(btnObject, 'kill');
            btnObject.switchPhaseFactory('DISABLED')();
            return;
        }
        if (keyword && keyword === 'kill' && !btnObject.animationsSetup) {
            btnObject.toggleOpacityCycle(btnObject, 'kill');
            return;
        }
        btnObject.inactiveTimeout = setTimeout(btnObject.showInactiveScreen, btnObject.inactiveLength * 1000, btnObject);
    },



    seekToEndOfActiveTimeline: function(btnObject, masterTimeline) {
        var children = masterTimeline.getChildren(false, true, true);
        // pause timelines that haven't happened yet
        for (var childIndex = children.length - 1; childIndex >= 0; childIndex--) {
            var currentTimeline = children[childIndex];
            if (currentTimeline.isActive() === true) {
                currentTimeline.progress(1);
            }
            if (currentTimeline.isActive() === false) {
                currentTimeline.pause();
            }
        }
    },

    handleClick: function(btnObject, forcePhase) {
        // if during HANDINCENTER phase:
        // 1)
        //      corners disappear,
        //      hide handsvg,
        //      show awesometextsvg
        //      activate sunflower
        // 2)
        //      slide awesometextsvg out
        //      slide clickanytime in


        // if during HANDONBOTTOM:
        // 1)
        //      hide handsvg,
        //      show awesometextsvg
        //      activate sunflower
        // 2)
        //      slide awesometextsvg out
        //      slide clickanytime in


        // GENERAL ANALYTICS INSTRUCTIONS:
        // pass state as interactionId (first parameter)
        // player.analytics.interaction(currentPhase, 'click' , {trigger: 'click' , action: 'click'});
        // TODO - simulated clicks don't trigger
        btnObject.ctx.player.analytics.interaction(btnObject.currentPhase, 'click', { trigger: 'click', action: 'other' });

        btnObject.toggleOpacityCycle(btnObject, 'kill');
        btnObject.restartInactiveTimeout(btnObject);

        btnObject.seekToEndOfActiveTimeline(btnObject, btnObject.timeline0Master);
        btnObject.animate(btnObject.ui.toprightcorner, 0, { top:this.options.borderinitial, right:this.options.borderinitial });
        btnObject.animate(btnObject.ui.toprightcornerCopy, 0, { top:this.options.borderinitial, right:this.options.borderinitial });
        btnObject.animate(btnObject.ui.bottomleftcorner, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial });
        btnObject.animate(btnObject.ui.bottomleftcornerCopy, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial });

        var handleClickTimeline = new window.TimelineMax();

        if (btnObject.currentPhase === 'HANDSLIDINGDOWN') {
            btnObject.currentPhase = 'HANDONBOTTOM';
        }

        switch (btnObject.currentPhase) {
            case 'INIT': {
                // do nothing
                break;
            }
            case 'RENDERED': {
                // do nothing

                break;
            }
            case 'BEGINONBOARD': {
                handleClickTimeline.to(btnObject.ui.handwithoutsunflower, 0, { display: 'none' })
                handleClickTimeline.to(btnObject.ui.handwithoutsunflowerCopy, 0, { display: 'none' })
                                    .to(btnObject.ui.toprightcorner, 0, { top:this.options.borderinitial, right:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.toprightcornerCopy, 0, { top:this.options.borderinitial, right:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.bottomleftcorner, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.bottomleftcornerCopy, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.clicktextsvg, 0, { display: 'none', immediateRender: false })
                                    .to(btnObject.ui.clicktextsvgCopy, 0, { display: 'none', immediateRender: false })
                                    .to(btnObject.ui.awesometextsvg, 0, { y:80, display:'initial', immediateRender: false })
                                    .to(btnObject.ui.awesometextsvgCopy, 0, { y:80, display:'initial', immediateRender: false })
                                    .to(btnObject.ui.clickanytimesvg, 0, { display:'initial', immediateRender: false })
                                    .to(btnObject.ui.clickanytimesvgCopy, 0, { display:'initial', immediateRender: false })
                                    .add(btnObject.animate(btnObject.ui.awesometextsvg, 0.5, { y:60, ease: 'Power4.easeIn' }), 1)
                                    .add(btnObject.animate(btnObject.ui.awesometextsvgCopy, 0.5, { y:60, ease: 'Power4.easeIn' }), 1)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvg, 0.4, { y:0, ease: 'Power4.easeOut' }), 1.6)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvgCopy, 0.4, { y:0, ease: 'Power4.easeOut' }), 1.6)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvg, 0.4, { y:80, ease: 'Power4.easeIn' }), 4)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvgCopy, 0.4, { y:80, ease: 'Power4.easeIn' }), 4);
                break;
            }
            case 'HANDINCENTER': {
                handleClickTimeline.to(btnObject.ui.handwithoutsunflower, 0, { display: 'none' });
                handleClickTimeline.to(btnObject.ui.handwithoutsunflowerCopy, 0, { display: 'none' })
                                    .to(btnObject.ui.toprightcorner, 0, { top:this.options.borderinitial, right:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.toprightcornerCopy, 0, { top:this.options.borderinitial, right:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.bottomleftcorner, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.bottomleftcornerCopy, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.clicktextsvg, 0, { display: 'none', immediateRender: false })
                                    .to(btnObject.ui.clicktextsvgCopy, 0, { display: 'none', immediateRender: false })
                                    .to(btnObject.ui.awesometextsvg, 0, { display:'initial', immediateRender: false })
                                    .to(btnObject.ui.awesometextsvgCopy, 0, { display:'initial', immediateRender: false })
                                    .to(btnObject.ui.clickanytimesvg, 0, { y:80, display:'initial', immediateRender: false })
                                    .to(btnObject.ui.clickanytimesvgCopy, 0, { y:80, display:'initial', immediateRender: false })
                                    .add(btnObject.animate(btnObject.ui.awesometextsvg, 0.5, { y:60, ease: 'Power4.easeIn' }), 1)
                                    .add(btnObject.animate(btnObject.ui.awesometextsvgCopy, 0.5, { y:60, ease: 'Power4.easeIn' }), 1)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvg, 0.4, { y:0, ease: 'Power4.easeOut' }), 1.6)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvgCopy, 0.4, { y:0, ease: 'Power4.easeOut' }), 1.6)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvg, 0.4, { y:80, ease: 'Power4.easeIn' }), 4)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvgCopy, 0.4, { y:80, ease: 'Power4.easeIn' }), 4);
                handleClickTimeline.add(btnObject.handSunflowerExplosion(btnObject, 'awesome'), 0);
                break;
            }

            case 'HANDSLIDINGDOWN': {
                // TODO: what happens here?
                break;
            }

            case 'HANDONBOTTOM': {
                handleClickTimeline.to(btnObject.ui.handwithoutsunflower, 0, { display: 'none' });
                handleClickTimeline.to(btnObject.ui.handwithoutsunflowerCopy, 0, { display: 'none' })
                                    .to(btnObject.ui.toprightcorner, 0, { top:this.options.borderinitial, right:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.toprightcornerCopy, 0, { top:this.options.borderinitial, right:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.bottomleftcorner, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.bottomleftcornerCopy, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.awesometextsvg, 0, { display:'initial' })
                                    .to(btnObject.ui.awesometextsvgCopy, 0, { display:'initial' })
                                    .to(btnObject.ui.clickanytimesvg, 0, { y:80, display:'initial' })
                                    .to(btnObject.ui.clickanytimesvgCopy, 0, { y:80, display:'initial' })
                                    .add(btnObject.handSunflowerExplosion(btnObject, 'awesome'), 0)
                                    .add(btnObject.animate(btnObject.ui.awesometextsvg, 0.5, { y:60, ease: 'Power4.easeIn' }), 1)
                                    .add(btnObject.animate(btnObject.ui.awesometextsvgCopy, 0.5, { y:60, ease: 'Power4.easeIn' }), 1)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvg, 0.4, { y:0, ease: 'Power4.easeOut' }), 1.6)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvgCopy, 0.4, { y:0, ease: 'Power4.easeOut' }), 1.6)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvg, 0.4, { y:80, ease: 'Power4.easeIn' }), 4)
                                    .add(btnObject.animate(btnObject.ui.clickanytimesvgCopy, 0.4, { y:80, ease: 'Power4.easeIn' }), 4);
                break;
            }
            case 'INACTIVEHAND': {
                handleClickTimeline.to(btnObject.ui.handwithoutsunflower, 0, { display: 'none' });
                handleClickTimeline.to(btnObject.ui.handwithoutsunflowerCopy, 0, { display: 'none' })
                                    .to(btnObject.ui.toprightcorner, 0, { top:this.options.borderinitial, right:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.toprightcornerCopy, 0, { top:this.options.borderinitial, right:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.bottomleftcorner, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial }, 0)
                                    .to(btnObject.ui.bottomleftcornerCopy, 0, { bottom:this.options.borderinitial, left:this.options.borderinitial }, 0)
                btnObject.toggleOpacityCycle(btnObject, 'kill');
                btnObject.inactiveTimeline.pause();
                break;
            }
            case 'DISABLED': {
                break;
            }
        }
        btnObject.switchPhaseFactory('DISABLED', 607)();
        btnObject.handleClickTimeline = handleClickTimeline;
        return handleClickTimeline;
    },

    showInactiveScreen: function(btnObject) {
        console.log('inside showInactiveScreen', btnObject.currentPhase);
        if (btnObject.currentPhase === 'OFFSCREEN') {
            return;
        }
        if (btnObject.currentPhase === 'DISABLED') {
            btnObject.inactiveTimeline.restart().play();
        }
        if (btnObject.currentPhase !== 'RENDERED' &&
            btnObject.currentPhase !== 'HANDONBOTTOM' &&
            btnObject.currentPhase !== 'HANDINCENTER' &&
            btnObject.currentPhase !== 'HANDSLIDINGDOWN') {
            setTimeout(function(btnObject) {
                if (btnObject.currentPhase === 'INACTIVEHAND') {
                    btnObject.restartInactiveTimeout(btnObject, 'kill');
                }
            }, 5000, btnObject);
        }
    },

    makeTimelineInactiveHand: function(btnObject) {
        var inactiveHandTimeline = new window.TimelineMax();

        inactiveHandTimeline
                            .call(function() { console.log('starting inactiveTimeline')})
                            .to(btnObject.ui.handsvg, 0, { y: btnObject.handDivFinish, immediateRender: false }, 0.1)
                            .to(btnObject.ui.handsvgCopy, 0, { y: btnObject.handDivFinish, immediateRender: false }, 0.1)
                            .to(btnObject.ui.handwithoutsunflower, 0, { display: 'initial', immediateRender: false }, 0.1)
                            .to(btnObject.ui.handwithoutsunflowerCopy, 0, { display: 'initial', immediateRender: false }, 0.1)
                            .to(btnObject.ui.handwithoutsunflower, 0, { opacity: 0, immediateRender: false }, 0.1)
                            .to(btnObject.ui.handwithoutsunflowerCopy, 0, { opacity: 0, immediateRender: false }, 0.1)
                            .to(btnObject.ui.handboundingbox, 0, { overflow: 'visible', immediateRender: false }, 0.1)
                            .to(btnObject.ui.handboundingboxCopy, 0, { overflow: 'visible', immediateRender: false }, 0.1)
                            .to(btnObject.ui.handanimbox, 0, { y: btnObject.handBottomY, scaleX: btnObject.handBottomScale, scaleY: btnObject.handBottomScale, immediateRender: false }, 0.1)
                            .to(btnObject.ui.handanimboxCopy, 0, { y: btnObject.handBottomY, scaleX: btnObject.handBottomScale, scaleY: btnObject.handBottomScale, immediateRender: false }, 0.1)
                            .to(btnObject.ui.handwithoutsunflower, 0.5, { opacity: 1 }, 0.1)
                            .to(btnObject.ui.handwithoutsunflowerCopy, 0.5, { opacity: 1 }, 0.1)
                            .call(btnObject.switchPhaseFactory('INACTIVEHAND', 649), [], btnObject, 0.1)
                            .call(btnObject.toggleOpacityCycle, [btnObject, 'forceplay'], btnObject, 0.1)

        inactiveHandTimeline.pause();
        return inactiveHandTimeline;
    },

    handSunflowerExplosion: function(btnObject, elementName) {
        var sunflowerGroup = btnObject.ui.sunflower;
        var sunflowerGroupCopy = btnObject.ui.sunflowerCopy;
        var masterExplosionTimeline = new window.TimelineMax({});

        function getRect(parent) {
            var rectNodes = [];
            var allNodes = parent.childNodes;
            for (var i = 0; i < allNodes.length; i++) {
                if (allNodes[i].tagName && allNodes[i].tagName.toUpperCase() === 'RECT') {
                    rectNodes.push(allNodes[i]);
                }
            }
            return rectNodes;
        }

        var rectChildren = getRect(sunflowerGroup);
        var rectChildrenCopy = getRect(sunflowerGroupCopy);

        var currentElem;

        // This is a hack to do the svg updates by hand in Firefox
        function updateSvgValue(element) {
            if (element && element.svgParamsToAnimate) {
                for (var paramName in element.svgParamsToAnimate) {
                    if (element.svgParamsToAnimate.hasOwnProperty(paramName) && paramName[0] !== '_') {
                        element[paramName].baseVal.value = element.svgParamsToAnimate[paramName];
                    }
                }
            }
        }

        for (var childIndex = 0; childIndex < rectChildren.length; childIndex++) {
            if (elementName === 'awesome' && [0,1,3,4,5,7].indexOf(childIndex) < 0) {
                continue;
            }
            currentElem = rectChildren[childIndex];
            var currentExplosion = new window.TimelineLite({ });

            currentElem.svgParamsToAnimate = {
                height: 36,
                y: 0
            };
            currentExplosion
                            .to(currentElem, 0, { visibility:'initial', immediateRender:false })
                            .to(currentElem, 0, { rotation: childIndex * 45, svgOrigin: '300 700', immediateRender:false })
                            .to(currentElem.svgParamsToAnimate, 0, { height: 20, y: 0, ease: 'Power1.easeIn', immediateRender:true, onUpdate: updateSvgValue, onUpdateParams:[currentElem] })
                            .to(currentElem.svgParamsToAnimate, 0.25, { height: 200, y: -300, ease: 'Power1.easeIn', onUpdate: updateSvgValue, onUpdateParams:[currentElem] })
                            .to(currentElem.svgParamsToAnimate, 0.4, { height: 0, y: -500, ease: 'Power4.easeOut', onUpdate: updateSvgValue, onUpdateParams:[currentElem] });
            masterExplosionTimeline.add(currentExplosion, 0);
        }
        for (var childIndexCopy = 0; childIndexCopy < rectChildrenCopy.length; childIndexCopy++) {
            if (elementName === 'awesome' && [0,1,3,4,5,7].indexOf(childIndexCopy) < 0) {
                continue;
            }
            currentElem = rectChildrenCopy[childIndexCopy];
            var currentExplosionCopy = new window.TimelineLite({ });

            currentElem.svgParamsToAnimate = {
                height: 36,
                y: 0
            };
            currentExplosionCopy
                            .to(currentElem, 0, { visibility:'initial', immediateRender:false })
                            .to(currentElem, 0, { rotation: childIndexCopy * 45, svgOrigin: '300 700', immediateRender:false })
                            .to(currentElem.svgParamsToAnimate, 0, { height: 20, y: 0, ease: 'Power1.easeIn', immediateRender:true, onUpdate: updateSvgValue, onUpdateParams:[currentElem] })
                            .to(currentElem.svgParamsToAnimate, 0.25, { height: 200, y: -300, ease: 'Power1.easeIn', onUpdate: updateSvgValue, onUpdateParams:[currentElem] })
                            .to(currentElem.svgParamsToAnimate, 0.4, { height: 0, y: -500, ease: 'Power4.easeOut', onUpdate: updateSvgValue, onUpdateParams:[currentElem] });
            masterExplosionTimeline.add(currentExplosionCopy, 0);
        }
        // masterExplosionTimeline.addCallback(setExploding, '+=0', [btnObject, false]);
        return masterExplosionTimeline;
    },

    switchPhaseFactory: function(newPhase, sender) {
        return function() {
            if (this.currentPhase === 'DISABLED' && (newPhase === 'HANDINCENTER' || newPhase === 'HANDONBOTTOM')) {
                return;
            }
            if (this.currentPhase !== newPhase) {
                if (newPhase === 'INACTIVEHAND') {
                    console.log('FORCING VIEW INACTIVEHAND');
                    this.ui.handwithoutsunflower.style.display = 'initial';
                    this.ui.handwithoutsunflowerCopy.style.display = 'initial';
                }
                console.log('ENTERING PHASE', newPhase, 'FROM', sender);
                this.currentPhase = newPhase;
            }
        }.bind(this);
    },

    makeTimelineShowCornersAndHand: function(btnObject) {
        var cornersAndHandTimeline = new window.TimelineMax({ delay:btnObject.delayBeforeOnboarding, onComplete: btnObject.switchPhaseFactory('HANDINCENTER', 553) });
        cornersAndHandTimeline
                        .call(btnObject.switchPhaseFactory('BEGINONBOARD', 0))
                        .add('comeintoview', 0)
                        // .to(btnObject.ui.handwithoutsunflower, 0, { display: 'initial' }, 'comeintoview')
                        // .to(btnObject.ui.handwithoutsunflowerCopy, 0, { display: 'initial' }, 'comeintoview')
                        // animate stroke width from 0 to too big and then to normal
                        .to(btnObject.ui.clicktextsvg, 0, { display: 'initial', immediateRender: false }, 'comeintoview')
                        .to(btnObject.ui.clicktextsvgCopy, 0, { display: 'initial', immediateRender: false }, 'comeintoview')
                        .to(btnObject.ui.toprightcorner, 0.8, { delay:0.1, top:this.options.borderdistance,
                                                                right:this.options.borderdistance, ease: 'Power4.easeOut' }, 'comeintoview')
                        .to(btnObject.ui.toprightcornerCopy, 0.8, { delay:0.1, top:this.options.borderdistance,
                                                                right:this.options.borderdistance, ease: 'Power4.easeOut' }, 'comeintoview')
                        .to(btnObject.ui.bottomleftcorner, 0.8, { delay:0.1, bottom:this.options.borderdistance,
                                                                left:this.options.borderdistance, ease: 'Power4.easeOut' }, 'comeintoview')
                        .to(btnObject.ui.bottomleftcornerCopy, 0.8, { delay:0.1, bottom:this.options.borderdistance,
                                                                left:this.options.borderdistance, ease: 'Power4.easeOut' }, 'comeintoview')
                        // animate in the hand and the text
                        .to(btnObject.ui.handsvg, 0.6, { y: btnObject.handDivFinish, ease: 'Power4.easeOut', }, 'comeintoview')
                        .to(btnObject.ui.handsvgCopy, 0.6, { y: btnObject.handDivFinish, ease: 'Power4.easeOut', }, 'comeintoview')
                        .to(btnObject.ui.clicktextContainer, 0.77, { delay:0.15, y: btnObject.clickDivFinish, ease: 'Power4.easeOut', }, 'comeintoview')
                        .to(btnObject.ui.clicktextContainerCopy, 0.77, { delay:0.15, y: btnObject.clickDivFinish, ease: 'Power4.easeOut', }, 'comeintoview')
                        // add the clicksplosion point
                        .add('clicksplosion', 1)
                        .addCallback(btnObject.toggleHandClick, 'clicksplosion', [btnObject])
                        .addCallback(btnObject.toggleHandClick, 'clicksplosion+=0.17', [btnObject])
                        .add(btnObject.handSunflowerExplosion(btnObject), 'clicksplosion');

        cornersAndHandTimeline.data = { identity : 'timeline1CornersAndHand' };
        btnObject.timeline1CornersAndHand = cornersAndHandTimeline;
        return cornersAndHandTimeline;
    },

    makeTimelineMoveHandToBottom: function(btnObject) {
        var handToBottomTimeline = new window.TimelineMax({ delay:btnObject.delayUntilHandMove });
        handToBottomTimeline
                            .call(btnObject.switchPhaseFactory('HANDSLIDINGDOWN', 576))
                            // Hide Click Text
                            .to(btnObject.ui.clicktextContainer, 0.5, { ease: 'Power3.easeIn', y: btnObject.clickDivStart }, 0)
                            .to(btnObject.ui.clicktextContainerCopy, 0.5, { ease: 'Power3.easeIn', y: btnObject.clickDivStart }, 0)
                            // Animate Stroke out
                            .to(btnObject.ui.toprightcorner, 0.6, { top:this.options.borderinitial,
                                                                    right:this.options.borderinitial, ease: 'Power4.easeOut' }, 0)
                            .to(btnObject.ui.toprightcornerCopy, 0.6, { top:this.options.borderinitial,
                                                                    right:this.options.borderinitial, ease: 'Power4.easeOut' }, 0)
                            .to(btnObject.ui.bottomleftcorner, 0.6, { bottom:this.options.borderinitial,
                                                                    left:this.options.borderinitial, ease: 'Power4.easeOut' }, 0)
                            .to(btnObject.ui.bottomleftcornerCopy, 0.6, { bottom:this.options.borderinitial,
                                                                    left:this.options.borderinitial, ease: 'Power4.easeOut' }, 0)
                            // Animate hand down
                            .to(btnObject.ui.handboundingbox, 0, { overflow: 'visible' })
                            .to(btnObject.ui.handboundingboxCopy, 0, { overflow: 'visible' })
                            .to(btnObject.ui.handanimbox, 0.7, { y: btnObject.handBottomY, scaleX: btnObject.handBottomScale, scaleY: btnObject.handBottomScale, ease: 'Power4.easeInOut' }, 0.15)
                            .to(btnObject.ui.handanimboxCopy, 0.7, { y: btnObject.handBottomY, scaleX: btnObject.handBottomScale, scaleY: btnObject.handBottomScale, ease: 'Power4.easeInOut' }, 0.15)
                            // Show text
                            .to(btnObject.ui.messagetextdiv, 1, { height: 110 }, 2)
                            .to(btnObject.ui.messagetextdivCopy, 1, { height: 110 }, 2)
                            .addCallback(btnObject.restartInactiveTimeout, '+=0', [btnObject])
                            .call(btnObject.switchPhaseFactory('HANDONBOTTOM', 583))
                            .call(btnObject.toggleOpacityCycle, [btnObject, 'play']);
        btnObject.timeline2HandToBottom = handToBottomTimeline;
        handToBottomTimeline.data = { identity : 'timeline2HandToBottom' };
        return handToBottomTimeline;
    },

    toggleOpacityCycle: function(btnObject, keyword) {
        if (btnObject.opacityCycle) {
            if (keyword === 'forceplay') {
                btnObject.animate(btnObject.ui.handwithoutsunflower, 0, { display: 'initial' });
                btnObject.animate(btnObject.ui.handwithoutsunflowerCopy, 0, { display: 'initial' });
                btnObject.opacityCycle.restart();
                return;
            }
            if (keyword === 'play') {
                btnObject.opacityCycle.restart();
                return;
            }
            if (keyword === 'kill' || btnObject.opacityCycle.isActive()) {
                btnObject.opacityCycle.pause().progress(0);
                return;
            }
            btnObject.opacityCycle.restart();
            return;
        }
        if (keyword === 'kill') {
            return;
        }
        btnObject.opacityCycle = new window.TimelineMax({ repeat:-1, yoyo:true });
        btnObject.opacityCycle.to(btnObject.ui.handwithoutsunflower, btnObject.opacityCycleSpeed, { fill: '#AAAAAA', stroke: '#AAAAAA' }, 0);
        btnObject.opacityCycle.to(btnObject.ui.handwithoutsunflowerCopy, btnObject.opacityCycleSpeed, { fill: '#7A4547', stroke: '#7A4547' }, 0);
        return;
    },

    toggleHandClick: function(btnObject) {
        if (typeof btnObject.ui.pointerfinger0.toggleDown === 'undefined') {
            btnObject.ui.pointerfinger0.toggleDown = true;
        }

        var currentDelta = btnObject.ui.pointerfinger0.toggleDown ? btnObject.handClickDelta : -btnObject.handClickDelta;
        btnObject.ui.pointerfinger0.y.baseVal.value += currentDelta;
        btnObject.ui.pointerfinger1.x.baseVal.value += currentDelta;
        btnObject.ui.pointerfinger1.width.baseVal.value -= currentDelta;
        btnObject.ui.pointerfinger2.x.baseVal.value += currentDelta;
        btnObject.ui.pointerfinger2.width.baseVal.value -= currentDelta;
        btnObject.ui.pointerfinger0Copy.y.baseVal.value += currentDelta;
        btnObject.ui.pointerfinger1Copy.x.baseVal.value += currentDelta;
        btnObject.ui.pointerfinger1Copy.width.baseVal.value -= currentDelta;
        btnObject.ui.pointerfinger2Copy.x.baseVal.value += currentDelta;
        btnObject.ui.pointerfinger2Copy.width.baseVal.value -= currentDelta;

        btnObject.ui.pointerfinger0.toggleDown = !btnObject.ui.pointerfinger0.toggleDown;
    },

});
