define([   'resources/skins/menuBackgroundButton'],
function(menuBackgroundButton) {
    var skins = {
        menuBackgroundButton: menuBackgroundButton    
    };
    return skins;
});
