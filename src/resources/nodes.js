define([
    'resources/ivds'
    ], function(ivds) {
        var nodePrefix = 'i110_MindGames';
        var nodes = [
            {
                id: 'i110_MindGames._1_opening',
                source:ivds.vid_opening_PAD,
                data: {
                    decision: {
                        children: ['i110_MindGames._2_action']
                    },
                    graph: {
                        seekChild: 'i110_MindGames._2_action'
                    }
                }
            },
            {
                id: 'i110_MindGames._2_action',
                source:ivds.vid_track_select_RTS,
                data: {
                    decision: {
                        children: ['i110_MindGames._3_ending'],
                        endTime: -1.8
                    },
                    graph: {
                        seekChild: 'i110_MindGames._3_ending'
                    }
                }
            },
            {
                id: 'i110_MindGames._3_ending',
                source:ivds.vid_ending_PAD,
                data: {
                    decision: {
                    }
                }
            }
    ];
    return nodes;
    }
);
